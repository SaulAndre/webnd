from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Subscriber
from .forms import Subs_Form
# Create your views here.

def subscriber(request):
    return render(request, 'subscriber.html', {'form': Subs_Form()})

def check_email(request):
    if request.method == "GET":
        return HttpResponseRedirect("/subs/")
    else:
        email = request.POST['email']
        response_data["is_success"] = '@' in email and '.' in email
        response_data["is_exist"] = Subscriber.objects.filter(email=email).exists()
        return JsonResponse(response_data)