from django.urls import path
from .views import subscriber

urlpatterns = [
    path('', subscriber),
]
