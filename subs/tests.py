from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import subscriber

# Create your tests here.
class TestSubs(TestCase):
    def test_url_exist(self):
        response = Client().get('/subs/')
        self.assertEqual(response.status_code, 200)

    def test_url_using_index_func(self):
        found = resolve('/subs/')
        self.assertEqual(found.func, subscriber)

    def test_models_can_create_objects(self):
        test = Subscriber.objects.create(email='saulandreee@gmail.com', nama="Saul Andre", password='abcd1234')
        countStatus = Subscriber.objects.all().count()
        self.assertEqual(test.__str__(), ("saulandreee@gmail.com"))
        self.assertEquals(countStatus, 1)

    def test_form_validation_for_blank_items(self):
        form = SubscriberForm(data={'email': '', 'nama':'', 'password':''})
        self.assertFalse(form.is_valid())

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = SubscriberForm()
        self.assertIn('class="form-control"', form.as_p())

