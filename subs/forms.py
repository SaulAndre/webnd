from django import forms
from .models import Subscriber


class Subs_Form(forms.ModelForm):
    email = forms.CharField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Email', 'class': 'form-control', 'onkeyup':'checkEmail(this);'}))
    nama = forms.DateField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Nama', 'class': 'form-control'}))
    password = forms.DateField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Password', 'class': 'form-control'}))
        
    class Meta:
        model = Subscriber
        fields = ['email', 'nama', 'password',]
