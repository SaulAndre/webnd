// Dark and Light Theme
$(document).ready(function() {
  $(".dark-theme").click(function() {
    $(".body").addClass("dark-body");
    $(".card").addClass("text-white bg-dark mb-3");
    $(".btn")
      .addClass("btn-light")
      .removeClass("btn-outline-secondary");
    $(".navbar")
      .addClass("navbar-dark bg-dark")
      .removeClass("navbar-light bg-light");
    $(".form-control").addClass("dark-form");
    $(".h1").addClass("dark-title");
    $(".status").addClass("dark-status");
    $(".accordion").addClass("dark-accordion");
    $(".title").addClass("dark-titleAcc");
    $(".detail").addClass("dark-detail");
  });
  $(".light-theme").click(function() {
    $(".body").removeClass("dark-body");
    $(".card").removeClass("text-white bg-dark mb-3");
    $(".btn")
      .removeClass("btn-light")
      .addClass("btn-outline-secondary");
    $(".navbar")
      .removeClass("navbar-dark bg-dark")
      .addClass("navbar-light bg-light");
    $(".form-control").removeClass("dark-form");
    $(".h1").removeClass("dark-title");
    $(".status").removeClass("dark-status");
    $(".accordion").removeClass("dark-accordion");
    $(".title").removeClass("dark-titleAcc");
    $(".detail").removeClass("dark-detail");
  });
  $(".dark-theme").hover(function() {
    $(".body").addClass("dark-body");
    $(".card").addClass("text-white bg-dark mb-3");
    $(".btn")
      .addClass("btn-light")
      .removeClass("btn-outline-secondary");
    $(".navbar")
      .addClass("navbar-dark bg-dark")
      .removeClass("navbar-light bg-light");
    $(".form-control").addClass("dark-form");
    $(".h1").addClass("dark-title");
    $(".status").addClass("dark-status");
    $(".accordion").addClass("dark-accordion");
    $(".title").addClass("dark-titleAcc");
    $(".detail").addClass("dark-detail");
  });
  $(".light-theme").hover(function() {
    $(".body").removeClass("dark-body");
    $(".card").removeClass("text-white bg-dark mb-3");
    $(".btn")
      .removeClass("btn-light")
      .addClass("btn-outline-secondary");
    $(".navbar")
      .removeClass("navbar-dark bg-dark")
      .addClass("navbar-light bg-light");
    $(".form-control").removeClass("dark-form");
    $(".h1").removeClass("dark-title");
    $(".status").removeClass("dark-status");
    $(".accordion").removeClass("dark-accordion");
    $(".title").removeClass("dark-titleAcc");
    $(".detail").removeClass("dark-detail");
  });
});

// Accordion
$(document).ready(function() {
  $(".detail").hide();
  $(".aktivitas-wrapper .title").click(function() {
    $(".desc-aktivitas").slideToggle("slow");
  });
  $(".pengalaman-wrapper .title").click(function() {
    $(".desc-pengalaman").slideToggle("slow");
  });
  $(".prestasi-wrapper .title").click(function() {
    $(".desc-prestasi").slideToggle("slow");
  });
});

// Check email exist and valid

$(document).ready(function() {
  $(".email-exist").hide();
  $(".email").hide();
  $(".success").hide();
});

function checkEmail(element) {
  email = $(element).val();
  $.ajax({
    url: "/check-email/",
    data: {
      csrfmiddlewaretoken: $(element)
        .siblings("input[name='csrfmiddlewaretoken']")
        .val(),
      login: login
    },
    method: "POST",
    dataType: "json",
    success: function(data) {
      //console.log(data);
      if (data.is_success) {
        if (data.is_exist == false) {
          $(".succses").show();
        } else {
          $(".email-exist").show();
        }
      } else {
        $(".email").show();
      }
    }
  });
}

function disabledButton() {
  $("#submit").prop("disabled", true);
}
function enabledButton() {
  $("#submit").prop("disabled", false);
}
