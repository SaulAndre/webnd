from django.shortcuts import render
from .models import Status
from .forms import StatusForm
from django.http import HttpResponseRedirect
# Create your views here.


def index(request):
    if request.method == "POST":
        ini_form = StatusForm(request.POST)
        if ini_form.is_valid():
            ini_form.save()
            return HttpResponseRedirect("/")
    else:
        ini_form = StatusForm()
    dari_db = Status.objects.all()
    content = {'form': ini_form, 'data': dari_db}
    return render(request, 'landingPG.html', content)


def profile(request):
    return render(request, 'profile.html')
