from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index, profile
from .models import Status
from .forms import StatusForm


# Create your tests here.


class TestLandingPG(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_challenge_url_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_url_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_challenge_url_using_right_function(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_template_contains(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_challenge_url_using_right_templates(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        img_link = 'https://scontent.fcgk18-2.fna.fbcdn.net/v/t1.0-9/19420803_1701370099891532_2830396182202244264_n.jpg?_nc_cat=104&_nc_ht=scontent.fcgk18-2.fna&oh=354f40661edf65d71f30fe9bf41365dc&oe=5CB2FE4D'
        self.assertIn('1706023555', html_response, 'no npm')
        self.assertIn(img_link, html_response, 'no img')

    def test_models_can_create_objects(self):
        test = Status.objects.create(status='Baik kok, baik banget')
        countStatus = Status.objects.all().count()
        self.assertEqual(test.__str__(), ("Baik kok, baik banget"))
        self.assertEquals(countStatus, 1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control"', form.as_p())

    def test_lab5_post_and_render_the_result(self):
        test = 'Cinta'
        response_post = Client().post('/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response, "ga ada di modul")

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)


    def tearDown(self):
        self.selenium.quit()

    def test_input_form_and_submit(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        # find the forms fields and submit button
        forms = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_class_name('btn-outline-secondary')
        # fill the fields and then submiting
        forms.send_keys('Coba coba')
        submit.click()
        self.assertIn('Coba coba', selenium.page_source)
        # testData = Status.objects.all()
        profileBtn = selenium.find_element_by_id('profile')
        profileBtn.click()
        # webdriver.implicitly_wait(3)
        self.assertIn('Saul Andre Lumban Gaol', selenium.page_source)

    def test_form_fields_border_color_challenge(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        fields = selenium.find_element_by_class_name('form-control')
        # properties = selenium.execute_script('return window.getComputedStyle(arguments[0],null);', fields)
        # for property in properties:
        #     print(fields.value_of_css_property(property))
        # # self.assertEqual()
        self.assertEqual(fields.value_of_css_property('background-color'), "rgba(255, 255, 255, 1)")

    def test_submit_button_have_pointer_cursor_challenge(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        submitBtn = selenium.find_element_by_id("submit")
        pointer = submitBtn.value_of_css_property("cursor")
        self.assertEqual('pointer', pointer)

    def test_submit_button_exist_challenge(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        submitBtn = selenium.find_element_by_id('submit')
        self.assertIn(submitBtn.text, selenium.page_source)

    def test_name_inside_card_challenge(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile')

        cardElements = selenium.find_element_by_class_name('card-text').text
        self.assertIn(cardElements, selenium.page_source)
        



