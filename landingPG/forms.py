from django import forms
from .models import Status


class StatusForm(forms.ModelForm):
    status = forms.CharField(label='', widget=forms.TextInput(
        attrs={'placeholder': "apa kabar kamuh", 'class': 'form-control'}))

    class Meta:
        model = Status
        fields = ['status']
