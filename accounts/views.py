from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def signupMember(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            #User Logged On
            login(request, user)

    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'forms':form})

def loginMember(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            #User Logged in
            user = form.get_user()
            login(request, user)
            
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'forms':form})

def logoutMember(request):
    if request.method == "POST":
        logout(request)
        return HttpResponseRedirect('/')